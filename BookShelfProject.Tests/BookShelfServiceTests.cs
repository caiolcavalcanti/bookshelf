using Microsoft.VisualStudio.TestTools.UnitTesting;
using BookShelfProject.Services.BookShelfService;
using BookShelfProject.Models;
using System;
using System.Collections.Generic;
using Shouldly;
using BookShelfProject.Exceptions;
using System.Linq;

namespace BookShelfProject.Tests
{
    [TestClass]
    public class BookShelfServiceTests
    {


        private IBookShelfService _bookshelfService;
        private int capacity = 3;

        [TestInitialize]
        public void TestInitialize()
        {

            _bookshelfService = new BookShelfService(capacity);

            _bookshelfService.Add(new Book
            {
                Id = "694d2d19-7487-46ad-8c34-396476e96c7a", // would not hardcode of course but rather use Guid.NewGuid() method          
                Author = "H. P. Lovecraft",
                Title = "At the Mountains of Madness",
                ContentPage = "At the Mountains of Madness is a science fiction-horror novella by American author H. P. Lovecraft, written in February/March 1931 and rejected that year by Weird Tales editor Farnsworth Wright on the grounds of its length.[1] It was originally serialized in the February, March, and April 1936 issues of Astounding Stories. It has been reproduced in numerous collections.",
                Pages = new List<string> { new NLipsum.Core.Paragraph().ToString(), new NLipsum.Core.Paragraph().ToString(), new NLipsum.Core.Paragraph().ToString() }
            });

            _bookshelfService.Add(new Magazine
            {
                Id = "1f7e979d-90aa-42da-bd74-98ac8f16703e",
                PublicationDate = new DateTime(2011, 1, 5),
                Name = "Some Magazine Name No One Will Ever Read",
                ContentPage = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam imperdiet ligula urna, at mollis diam rutrum sed. Vestibulum fringilla imperdiet nibh ultricies porttitor. Aenean sodales efficitur felis, non ultrices nunc eleifend ut. Fusce metus nibh, tempor et tristique vitae, laoreet posuere augue. Cras sed vestibulum massa, vitae bibendum ex. Praesent pulvinar ante vel orci luctus, sed ultrices nisl semper. Duis et enim consequat, fermentum nisl a, interdum sem. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec pretium vehicula sapien at lacinia. Pellentesque malesuada sodales erat, eget malesuada massa ornare sed.",
                Pages = new List<string> { new NLipsum.Core.Paragraph().ToString(), new NLipsum.Core.Paragraph().ToString() }
            });

            _bookshelfService.Add(new Notebook
            {
                Id = "5ab2be54-bef4-4f8a-acb5-a2deef3fcb99",
                Owner = "Caio",
                ContentPage = "Vestibulum porttitor ultrices erat sit amet elementum. Praesent vel metus diam. Phasellus convallis magna quis eros vulputate, et fermentum lectus scelerisque. Pellentesque venenatis nec leo sit amet mollis. Donec posuere scelerisque nulla, a commodo lorem congue in. Cras volutpat gravida dolor eu mattis. Praesent non tellus scelerisque, aliquam urna et, porttitor nisl. Donec at arcu tristique, hendrerit dui eu, malesuada orci. Nulla facilisi. Curabitur sed sapien dolor.",
                Pages = new List<string> { "Quisque mattis ultricies vehicula. Quisque pharetra libero sed eros venenatis aliquam. Aenean id nisi et dui fringilla porttitor. Sed porta cursus metus, id iaculis purus scelerisque scelerisque. Curabitur non ipsum diam. Fusce consequat mi eu ex tristique convallis. Donec mollis lacus mi, in laoreet eros finibus sed. Duis sed turpis eget eros auctor dapibus eu sit amet justo. Praesent id iaculis tortor. Aenean egestas nec nibh ac pharetra." }
            });
        }

        [TestMethod]
        public void TestItemCount()
        {
            var itemCount = _bookshelfService.GetBookshelfCount();
            itemCount.ShouldBe(3);
        }

        [TestMethod]
        public void TestGetOne()
        {
            Book item = (Book)_bookshelfService.GetOne("694d2d19-7487-46ad-8c34-396476e96c7a");
            item.Title.ShouldBe("At the Mountains of Madness");
        }

        [TestMethod]
        [ExpectedException(typeof(NotEnoughRoomAvailableException))]
        public void TestAddingMoreItems()
        {
            _bookshelfService.Add(new Book
            {
                Id = Guid.NewGuid().ToString(),
                Author = "Emil Chioran",
                Title = "On the Heights of Despair",
                ContentPage = "On the Heights of Despair is a 1934 non-fiction book by philosopher Emil Cioran, that was translated from Romanian into English in 1996 by Ilinca Zarifopol-Johnston, who also wrote the book's English-language introduction. The first work that Cioran published, it is a series of short essays, dealing with themes that would later permeate his work, such as death, insomnia and insanity. On the Heights of Despair received the King Carol II Royal Foundation's Young Writer's Prize.[1] This was the only award that Cioran himself accepted – his later works would win the awards for the Prix Rogier Namier and Grand prix de littérature Paul-Morand, but he refused both of them.[2]",
                Pages = new List<string> { new NLipsum.Core.Paragraph().ToString(), new NLipsum.Core.Paragraph().ToString(), new NLipsum.Core.Paragraph().ToString() }
            });
        }

        [TestMethod]
        public void TestSearchFor()
        {
            var searchFor = "Lorem ipsum dolor sit amet";
            var result = _bookshelfService.Search(searchFor).FirstOrDefault();            
            result.ShouldBeOfType<Magazine>();
        }

        [TestMethod]
        [ExpectedException(typeof(ElementNotFoundException))]
        public void TestElementNotFound()
        {
            var item = _bookshelfService.GetOne(Guid.NewGuid().ToString());
        }

        [TestMethod]
        [ExpectedException(typeof(PageIndexOutOfBoundsException))]
        public void TestPageNumberOutOfBounds()
        {
            Book item = (Book)_bookshelfService.GetOne("694d2d19-7487-46ad-8c34-396476e96c7a");
            var page = item.GetPage(6);
        }

    }
}