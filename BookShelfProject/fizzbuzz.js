﻿<ul id="fizzbuzzareav1"></ul>
<ul id="fizzbuzzareav2"></ul>

      <script language="javascript">
          function fizzBuzz() {

              var printOn = document.getElementById("fizzbuzzareav1");

              for (var i = 1; i <= 100; i++) {

                  var element = document.createElement("li");
                  var content = "";

                  if (i % 3 == 0 && i % 5 == 0) {
                      content = document.createTextNode("FizzBuzz");
                  }
                  else if (i % 3 == 0) {
                      content = document.createTextNode("Fizz");
                  }
                  else if (i % 5 == 0) {
                      content = document.createTextNode("Buzz");
                  }
                  else {
                      content = document.createTextNode(i);
                  }
                  element.appendChild(content);
                  printOn.appendChild(element);

              }
          }

          function fizzBuzzV2() {

              var printOn = document.getElementById("fizzbuzzareav2");

              for (var i = 1; i <= 100; i++) {

                  var element = document.createElement("li");
                  var content = "";

                  if (i % 3 == 0) {
                      content = "Fizz";
                  }
                  if (i % 5 == 0) {
                      content += "Buzz";
                  }

                  if (content == "") {
                      content = i;
                  }

                  var add = document.createTextNode(content);

                  element.appendChild(add);
                  printOn.appendChild(element);

              }
          }
          fizzBuzz();
          fizzBuzzV2();
      </script>