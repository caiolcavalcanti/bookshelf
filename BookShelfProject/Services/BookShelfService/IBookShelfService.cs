﻿using BookShelfProject.Models;

namespace BookShelfProject.Services.BookShelfService
{   
    public interface IBookShelfService
    {
        IEnumerable<IBookShelfItem> GetAll();
        IBookShelfItem GetOne(string id);
        void Add(IBookShelfItem item);
        int GetBookshelfCount();
        int AmountLeftItCanHold();
        IEnumerable<IBookShelfItem> Search(string searchFor);
    }
}
