﻿using BookShelfProject.Exceptions;
using BookShelfProject.Models;

namespace BookShelfProject.Services.BookShelfService
{
    public class BookShelfService : IBookShelfService
    {

        private int Capacity;
        private List<IBookShelfItem> Items { get; set; }

        public BookShelfService(int capacity)
        {
            Items = new List<IBookShelfItem>();
            Capacity = capacity;
        }

        public int GetBookshelfCount()
        {
            return Items.Count;
        }

        public int AmountLeftItCanHold()
        {
            return Capacity - GetBookshelfCount();
        }

        public IEnumerable<IBookShelfItem> Search(string searchfor)
        {
            return Items.Where(p => p.ContentPage.Contains(searchfor));
        }

        public IEnumerable<IBookShelfItem> GetAll()
        {
            return Items;
        }

        public IBookShelfItem GetOne(string id)
        {
            var element = Items.Where(p => p.Id == id).FirstOrDefault();
            return element ?? throw new ElementNotFoundException();
        }

        public void Add(IBookShelfItem item)
        {
            if (AmountLeftItCanHold() > 0)
            {
                Items.Add(item);
            }
            else
            {
                throw new NotEnoughRoomAvailableException();
            }
        }
    }
}
