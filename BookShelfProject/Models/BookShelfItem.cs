﻿using BookShelfProject.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShelfProject.Models
{
    public class BookShelfItem : IBookShelfItem
    {

        private string _id;
        private IEnumerable<string> pages;
        private string _contentPage;

        public string Id { get => _id; set => _id = value; }
        public IEnumerable<string> Pages { get => pages; set => pages = value; }
        public string ContentPage { get => _contentPage; set => _contentPage = value; }

        public string GetPage(int pageNumber)
        {
            return Pages.Count() >= pageNumber + 1 ? Pages.ToList()[pageNumber + 1] : throw new PageIndexOutOfBoundsException();
        }

        public IEnumerable<string> Search(string searchFor)
        {
            return Pages.Where(p => p.Contains(searchFor));
        }

    }
}
