﻿using BookShelfProject.Models;

namespace BookShelfProject.Models
{
    public class Book : BookShelfItem
    {
        public string Title { get; set; }
        public string Author { get; set; }
    }
}
