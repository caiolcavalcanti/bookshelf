﻿namespace BookShelfProject.Models
{
    public class Magazine : BookShelfItem
    {

        public Magazine()
        {
            // todo: implement Guard
        }
        public string Name { get; set; }
        public DateTime PublicationDate { get; set; }
    }
}
