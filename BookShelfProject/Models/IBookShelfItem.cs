﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShelfProject.Models
{
    // initially called this IItem but it looked ugly so made it a bit verbose
    public interface IBookShelfItem
    {
        string Id { get; set; }
        IEnumerable<string> Pages { get; set; }
        string ContentPage { get; set; }
        IEnumerable<string> Search(string searchFor);
        string GetPage(int pageNumber);
    }
}
